;;;; Environment ADT
(defpackage environment
  (:use cl)
  (:export
   :make-empty-environment
   :extend-environment
   :environment-lookup
   :extend-environment-with-alist-bindings
   :get-identifiers))

(in-package :environment)


;;; Primitive implementation
(defstruct %environment (bindings nil))


;;; Low level API
(defun make-empty-environment ()
  "return a new empty environment"
  (make-%environment))


(defun get-bindings (environment)
  "return a list of bindings available in environment"
  (%environment-bindings environment))


(defun get-identifiers (environment)
  "Return a list of the identifiers in environment"
  (mapcar #'car (get-bindings environment)))


(defun extend-environment (id value environment)
  "return a new environment E which is the extension of `environment` with
`id` bound to `value`"
  (let ((env-list (%environment-bindings environment))
        (new-binding (cons id value)))
    (make-%environment :bindings (cons new-binding env-list))))


(defun environment-lookup (id env)
  "return the value of id in env"
  (let ((value (assoc id (%environment-bindings env) :test #'string=)))
    (and value (cdr value))))

;;; High level API
(defun extend-environment-with-alist-bindings (alist env)
  "return a new environment which is the result of extending env with
the bindings in alist.
Conditions: alist is an a-list with the form (id . value)"
  (if (null alist)
      env
      (let ((id (caar alist))
            (val (cdar alist))
            (rest (cdr alist)))
        (let ((new-env (extend-environment id val env)))
          (extend-environment-with-alist-bindings rest new-env)))))

