;;;; The interpreter

(defpackage interpreter
  (:use :cl :program :stack :environment)
  (:export :evaluate))

(in-package :interpreter)


(defun primitive-bindings ()
  "return a list of bindings for the primitive environment"
  (list (cons "+" (lambda (program) (eval-arithmetic-op #'+ program)))
        (cons "*" (lambda (program) (eval-arithmetic-op #'* program)))
        (cons "-" (lambda (program) (eval-arithmetic-op #'- program)))
        (cons "idiv" (lambda (program) (eval-arithmetic-op #'(lambda (a b)
                                                      (truncate (/ a b)))
                                                   program)))
        (cons "div" (lambda (program) (eval-arithmetic-op #'/ program)))
        (cons "expt" (lambda (program) (eval-arithmetic-op #'(lambda (a b)
                                                         (expt a b))
                                                     program)))
        (cons "dup" #'duplicate)
        (cons "swap" #'swap)
        (cons "over" #'over)
        (cons "drop" #'drop)
        (cons ";" #'bind)))


(defun make-primitive-environment ()
  "return a new environment containing only primitive bindings"
  (labels ((builder (xs env)
             (if (null xs)
                 env
                 (let ((id (caar xs))
                       (value (cdar xs))
                       (rest (cdr xs)))
                   (builder rest
                            (extend-environment id value env))))))
    (builder (primitive-bindings) (make-empty-environment))))


(defun evaluate (stack)
  "return the stack which results from evaluating stack"
  (get-stack (interprete-stack stack)))

  
(defun interprete-stack (stack)
  "return the value of interpreting stack"
  (eval-program (make-program stack (make-primitive-environment))))


(defun eval-program (program)
  "return a program P where P's stack cannot be simplified further given
P's environment"
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (if (emptyp stack)
        program
        (let ((fe (head stack))
              (st (tail stack)))
          (cond ((numberp fe)
                 (let ((new-program (eval-program (make-program st env))))
                   (push-on-program-stack fe new-program)))
                ((stringp fe)
                 (let ((func (environment-lookup fe env)))
                   (if func
                       (let ((new-program (make-program st env)))
                         (apply-operator func new-program))
                       (error "~&Undfined operator: ~s" fe))))
                ((stackp fe)
                 (let* ((new-program (eval-program (make-program fe env)))
                        (stack-2 (get-stack new-program))
                        (env-2 (get-environment new-program))
                        (stack-3 (stack-adjoin stack-2 st)))
                   (eval-program (make-program stack-3 env-2))))
                (t (error "~&Invalid program: ~s" stack)))))))


(defun apply-operator (operator program)
  "return a program P which results from applying operator to program"
  (if (functionp operator)
      (eval-program (funcall operator program))
      (error "~& Illegal operator ~s" operator)))

  
(defun drop (program)
  "return a new program P whose stack S is the same as program's with the 
first numeric value of program's stack dropped.
If the head of program's stack is not a number, then return the result of
calling drop on the program resulting from evaluating program.

Precondition: program's stack is not empty"
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (cond ((emptyp stack) (error "Stack empty! nothing to drop."))
          ((numberp (fst stack))
           (make-program (tail stack) env))
          (t (drop (eval-program program))))))

  
(defun duplicate (program)
  "return a new program P where P's stack is the stack of the input program
with the numeric element at the head duplicated.
If the first element of program's stack is not a number, program is first
evaluated, then duplicate is applied to the resulting program.

Precondition: program's stack is not empty"
  (let ((stack (get-stack program)))
    (cond ((emptyp stack)
           (error "Stack empty! nothing to duplicate."))
          ((numberp (fst stack))
           (push-on-program-stack (fst stack) program))
          (t (duplicate (eval-program program))))))


(defun swap (program)
  "let R be the result of evaluating program. 
SWAP returns the a program P which has R's stack but with the first two numbers swapped."
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (if (emptyp stack)
        (error "Stack empty! Nothing to swap")
        (let ((a (head stack)))
          (if (numberp a)
              (let ((st (tail stack)))
                (if (emptyp st)
                    (error "Singleton stack! SWAP requires 2 numbers.")
                    (let ((b (head st)))
                      (if (numberp b)
                          (let ((new-program (make-program (tail st) env)))
                            (push-on-program-stack b
                                                   (push-on-program-stack a new-program)))
                          (let ((new-program (eval-program (make-program st env))))
                            (swap (push-on-program-stack a new-program)))))))
              (swap (eval-program program)))))))
  

(defun over (program)
  "return a new program P such that the head of P's stack is the second element
of program's stack, and P's tail is program's stack after program has been 
evaluated"
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (if (emptyp stack)
        (error "Stack empty! OVER requires at least two elements.")
        (let ((a (head stack)))
          (if (numberp a)
              (let ((st (tail stack)))
                (if (emptyp st)
                    (error "Singelton stack! OVER requires 2 numbers.")
                    (let ((b (head st)))
                      (if (numberp b)
                          (push-on-program-stack b program)
                          (let ((new-program (eval-program (make-program st env))))
                            (over (push-on-program-stack a new-program)))))))
              (swap (eval-program program)))))))
  

;; Evaluating arithmetic operations.
;; let us reperesent programs as follows <stack, environment> and,
;; eval-arithmetic-op op program => program
;; let
;;   <(a b rest), env> = program
;; in
;;   if a is a number then,
;;     if b is a number then,
;;       let
;;         c = b `op` a
;;       in
;;         <(c rest),env>
;;       end
;;     else if b is a string then,
;;       let
;;         <(e gs), env2> = evaluate <(b rest), env>
;;       in
;;         eval-arithmetic-op op <(a e gs), env2>
;;       end
;;   else if a is a string, 
;;     let
;;       Pa = evaluate program
;;     in
;;       eval-aritmetic-op op Pa
;;     end

(defun eval-arithmetic-op (op program)
  "return a program P which results from applying op to program."
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (if (emptyp stack)
        (error "Stack empty! Arithmetic operators require 2 numbers.")
        (let ((a (head stack)))
          (if (numberp a)
              (let ((st (tail stack)))
                (if (emptyp st)
                    (error "Singleton stack! Arithmetic operators require 2 numbers.")
                    (let ((b (head st)))
                      (if (numberp b)
                          (push-on-program-stack (funcall op b a)
                                                 (make-program (tail st) env))
                          (let ((new-program (eval-program (make-program st env))))
                            (eval-arithmetic-op op
                                                (push-on-program-stack a new-program)))))))
              (eval-arithmetic-op op (eval-program program)))))))

  
;; Bindings
;; A primitive value in the language of definition is either
;; 1. a number
;; 2. a primitive procedure
;; 3. a stack in its simplest form
;;
;; For the sake of uniformity and simplicity, all bindings in
;; the environment will have the form <string, procedure>
;; 
;; Bindings come in a binding stack B which can have 2 forms.
;; FORM-1:
;;      <'value id>
;; FORM-2:
;;      <'v1 v2 .. vn id>
;; where <v1 v2 .. vn> can be numbers and or bound strings.
;;
;; First, we define the helper function make-operator
;; make-operator x = (lambda (program)
;;                     (if (functionp x) x
;;                         #'(lambda (program)
;;                             (push-on-program-stack x program))))
;; 
;; Let the program with the binding be <S,E>.
;;     In the case where the binding is in FORM-1,
;;     S = <value id>
;;     if value is a number, then
;;         <,extend E with id (make-operator value)>
;;     else if value is a string, then
;;       let
;;         v* be the value of value in E
;;       in
;;         if v* is defined, then
;;           <,extend E with id v*>
;;       end
;;     else
;;       error Illegal Binding form
;;
;;    When  the binding is in FORM-2,
;;    let
;;      S = < v1 v2 .. vn>
;;    in
;;      if every v in <v1 .. vn> is a number, then
;;        <, extend environment E with id (make-operator <v1 .. vn>)>
;;      else if every v in <v1 .. vn> is a string bound in E then,
;;        <,extend E with id compose v1 .. vn>
;;      else
;;        let
;;          <V,W> = eval-prog <v1 .. vn,E>
;;        in
;;          <,extend W with id (make-operator V)
;;        end
;; end
;;
(defun bind  (program)
  "return a program <,E> where the binding at the head of program is
established in E"
  (let* ((vs-id (get-binding (get-stack program)))
         (vs (head vs-id))
         (id (head (tail vs-id)))
         (env (get-environment program)))
    (if (singletonp vs)
        (let ((v (head vs)))
          (cond ((numberp v)
                 (let* ((val (make-operator v))
                       (new-env (extend-environment id val env)))
                   (make-program-with-empty-stack new-env)))
                ((stringp v)
                 (let ((val (environment-lookup v env)))
                   (if val
                       (let ((new-env (extend-environment id val env)))
                         (make-program-with-empty-stack new-env))
                       (error "Unbound operator ~S" v))))
                (t (error "Illegal value for binding ~s" v))))
        (cond ((stack-all-p #'numberp vs)
               (let* ((val (make-operator vs))
                      (new-env (extend-environment id val env)))
                 (make-program-with-empty-stack new-env)))
              ((stack-all-p #'stringp vs)
               (let* ((val (effective-function vs env))
                      (new-env (extend-environment id val env)))
                 (make-program-with-empty-stack new-env)))
              (t (let* ((new-program (eval-program (make-program vs env)))
                        (v (get-stack new-program))
                        (val (make-operator v))
                        (env-2 (get-environment new-program))
                        (new-env (extend-environment id val env-2)))
                   (make-program-with-empty-stack new-env)))))))



(defun get-binding (stack)
  "return a stack of containing only two elements.
The first element of the returned stack is a stack of the values for a binding and
the second element of the stack is string which the new value will be bound to."
  (when (or (emptyp stack) (singletonp stack))
    (error "Illegal binding form! Bindings must have an ID and at least one Value"))
  (unless (string= ":" (stack-last stack))
    (error "Illegal binding form! Binding syntax is: \": ID VALUE ;\""))
  (let* ((vs-id (but-last stack))
         (id (stack-last vs-id))
         (vs (but-last vs-id)))
    (cond ((numberp id)
           (error "Illegal binding form! cannot redefine a number"))
          (t (make-stack vs id)))))


(defun make-operator (x)
  "If x is a function, return x.
Else, return a function which takes a program pushes x on the program's stack"
  (if (functionp x)
      x
      #'(lambda (program)
          (push-on-program-stack x program))))


(defun effective-function (stack env)
  "return the composition of all the funtions in stack.
Preconditions: every element of stack is a variable in env"
  (compose (mapcar #'(lambda (x) (environment-lookup x env))
                   (get-stack-contents stack))))


(defun compose (func-list)
  "return a function which is the composition of the function in func-list"
  (labels ((compose-2 (f g)
             #'(lambda (x)
                 (funcall f (funcall g x)))))
    (reduce #'compose-2 func-list)))
