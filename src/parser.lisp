;;;; Stacky Parser

(defpackage parser
  (:use :cl :stack)
  (:export
   :string-list->stack
   :string->stack))

(in-package :parser)


(defun get-index-in-string (character string)
  "return the index of chararcter in string. If chararacter is not in string return nil"
  (let ((len (length string)))
    (labels ((finder (idx)
               (unless (= idx len)
                 (if (char= (char string idx) character)
                     idx
                     (finder (1+ idx))))))
      (finder 0))))

(defun float-readable-p (string)
  "return a float if `string` can be interpreted as a float else return nil."
  (when (and (= 1 (count #\. string :test #'char=))
             (every #'digit-char-p (remove #\. string)))
    (let* ((pos (get-index-in-string #\. string))
           (whole-part (subseq string 0 pos))
           (fractional-part (subseq string (1+ pos)))
           (div-power (length fractional-part)))
      (let ((whole-int (or (and (zerop (length whole-part))
                                0)
                           (parse-integer whole-part)))
            (fraction (* 1.0
                        (or (and (zerop div-power)
                                 0)
                            (/ (parse-integer fractional-part)
                               (expt 10 div-power))))))
        (+ whole-int fraction)))))


(defun char-list->stacky-value (char-list)
  "return the stacky value resulting from revesing the concatenated elements of char-list.
A value is either a number or a symbol."
  (let* ((str (nreverse (coerce char-list 'string)))
         (float-num (float-readable-p str)))
    (cond (float-num float-num)
          ((every #'digit-char-p str)
           (parse-integer str))
          (t str))))


(defun push-val (char-list stack)
  "return a new stack which has the forth value resulting from char-list->value stack
on stack."
  (if (null char-list)
      stack
      (push-stack (char-list->stacky-value char-list) stack)))


(defun string->stack (str)
  "return a stack of S of values in the Stacky language.
The elements of S are the value equivalent of the words in str pushed onto
S in thier order of appearance.

A word is a contigiuos sequence of characters not including any blank."
  (let ((len (length str)))
    (labels ((parser (idx acc stack)
               (if (= idx len)
                   (push-val acc stack)
                   (let ((c (char-downcase (char str idx)))
                         (next (1+ idx)))
                     (if (char= c #\space)
                         (parser next nil (push-val acc stack))
                         (parser next (cons c acc) stack))))))
      (parser 0 nil (make-stack)))))


(defun string-list->stack (string-list)
  "return a stack S which consists of the stacks represented by string-list.
S's head is the first element of string-list and S's tail is the CDR of string-list."
  (list->stack (mapcar #'string->stack string-list)))
