;;;; Program ADT
;;;; A program is a datastructure which holds astack and an environment together

(defpackage program
  (:use :cl)
  (:import-from :stack :push-stack :make-stack)
  (:export :make-program
           :make-program-with-empty-stack
           :get-stack
           :get-environment
           :push-on-program-stack))

(in-package :program)


;; Representation
(defstruct %program (stack nil) (environment nil))

;; API
(defun make-program (stack environment)
  "return a new program consisting of stack and environment"
  (make-%program :stack stack :environment environment))


(defun get-stack (program)
  "return the stack in environment"
  (%program-stack program))


(defun get-environment (program)
  "return the stack in environment"
  (%program-environment program))


(defun push-on-program-stack (element program)
  "return a new program resulting from pushing element on program's stack"
  (let ((stack (get-stack program))
        (env (get-environment program)))
    (make-program (push-stack element stack) env)))

(defun make-program-with-empty-stack (environment)
  "return a new program with environment which and the empty stack"
  (make-program (make-stack) environment))
