;;;; Stack ADT
(defpackage stack
  (:use cl)
  (:export
   :stackp
   :make-stack
   :make-stack
   :get-stack-contents
   :list->stack
   :emptyp
   :head
   :tail
   :singletonp
   :fst
   :snd
   :push-stack
   :but-last
   :stack-last
   :stak-foldl
   :stack-reverse
   :stack-adjoin
   :stack-all-p
   :stack-compose))

(in-package :stack)


;; Primitive representation
(defstruct %stack (contents nil))


;;Predicate
(defun stackp (x)
  "return t iff x is a stack"
  (%stack-p x))


;; Accessor
(defun get-stack-contents (stack)
  "return a list of the contents of stack"
  (%stack-contents stack))


;; Creation
(defun make-stack (&rest elements)
  "return a new stack with elements as its contents"
  (make-%stack :contents elements))


(defun list->stack (list)
  (apply #'make-stack list))


;;; Stack Operations
;;; Every function from here is independent of the primitive implimentation.
(defun emptyp (stack)
  "return t iff stack is empty"
  (null (get-stack-contents stack)))


(defun head (stack)
  "return the head of stack"
  (let ((list (get-stack-contents stack)))
    (if (null list)
        (error "Stack empty.")
        (first list))))


(defun tail (stack)
  "return a stack S with the head of stack removed."
    (let ((list (get-stack-contents stack)))
    (if (null list)
        (error "Stack empty.")
        (list->stack (cdr list)))))


(defun singletonp (stack)
  "return t iff stack contains at least 2 elements"
  (let ((s (get-stack-contents stack)))
    (unless (null s)
      (null (cdr s)))))


(defun fst (stack)
  "return the first element in stack"
  (head stack))


(defun snd (stack)
  "return the second element in stack"
  (head (tail stack)))


(defun push-stack (element stack)
  "return a new stack S with element at the head and the contents of stack
as the tail"
  (list->stack (cons element (get-stack-contents stack))))


(defun but-last (stack)
  "return a new stack S contains all elements of stack execept its last element"
  (labels ((rec (list acc)
             (if (null (cdr list))
                 (nreverse acc)
                 (rec (cdr list) (cons (car list) acc)))))
    (let ((s (get-stack-contents stack)))
      (if (null s)
          (error "BUT-LAST is undefined for empty stacks!")
          (list->stack (rec s nil))))))

(defun stack-last (stack)
  "return the last element in stack"
  (if (emptyp stack)
      (error "STACK-LAST is undefined for empty stacks!")
      (head (stack-reverse stack))))


(defun stack-foldl (func initial-value stack)
  "the foldl function defined on stacks"
  (if (emptyp stack)
      initial-value
      (stack-foldl func
                   (funcall func initial-value (fst stack))
                   (tail stack))))


(defun stack-reverse (stack)
  "return the reverse of stack"
  (stack-foldl #'(lambda (acc x) (push-stack x acc))
               (make-stack)
               stack))


(defun stack-adjoin (s1 s2)
  "return a new stack S which consists of the elements of s1 and s2 in order"
  (stack-foldl #'(lambda (acc x) (push-stack x acc))
               s2
               (stack-reverse s1)))


(defun stack-all-p (predicate stack)
  "return t iff predicate is true for every element in stack"
  (every predicate (get-stack-contents stack)))


(defun stack-compose (stack)
  "return a function which is the composition of all funtions in stack.
Precondition: every element in stack is a function of one argument."
  (flet ((compose-2 (f g)
           #'(lambda (x) (funcall f (funcall g x)))))
    (stack-foldl #'compose-2 #'identity stack)))
