;;;; Stacky, the user interface

(defpackage Stacky
  (:use :cl :interpreter :parser)
  (:export :stacky))

(in-package :stacky)


(defun stacky (string-list)
  "return the result of evaluating the program given by string-list"
  (evaluate (string-list->stack string-list)))
