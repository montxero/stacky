;;;; System definition for Stacky

(asdf:defsystem #:stacky
  :author "Seyi MontXero"
  :description "A simple stack based programming language"
  :depends-on nil
  :serial t
  :components ((:module "src"
                :serial t
                :components ((:file "stack")
                             (:file "environment")
                             (:file "program")
                             (:file "interpreter")
                             (:file "parser")
                             (:file "stacky")))))
